
# KeyMaster
Are you struggling to manage all of your keys and environment variables scattered across various systems?  If so, the KeyMaster has your back. 

By using LastPass Premium as a backend, you can securely manage all of your environment variables / keys. The KeyMaster package lets you easily & securely interact with the LastPass API and access your keys whenever and wherever you need them.

# Installation
```bash
pip install git+https://gitlab.com/advancedmd_data_team/keymaster
```

## Setup
To get started, add your LastPass credentials in the following format at the bottom of your .env file:
```bash
...
# LastPass creds (used by KeyMaster)
LASTPASS_EMAIL=your_email@gmail.com
LASTPASS_PASSWORD=your_password_here
```

## Usage
```python
from keymaster import KeyMaster

km = KeyMaster()

# Show all the keys available for pulling
km.show_keys()
# returns a list of available keys

# Request value by key
km.get('<<<SOME KEY HERE>>>')
# returns a thing
```

Happy coding!
