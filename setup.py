from setuptools import setup, find_packages

setup(
    name="keymaster",
    version="0.1",
    description="A simple wrapper around the LastPass API.",
    url="https://gitlab.com/advancedmd_data_team/keymaster",
    author="David Ostler",
    author_email="david.ostler001@gmail.com",
    license=None,
    packages=find_packages(exclude=["tests", ".idea", ".cache", "__pycache__"]),
    install_requires=["python-dotenv", "lastpass-python"],
    include_package_data=True,
    zip_safe=False,
)
