import lastpass
import os
from dotenv import load_dotenv, find_dotenv
import logging


class KeyMaster:
    def __init__(self):
        # Load the environment containing LastPass creds (.env file)
        env_path = find_dotenv()
        logging.debug(".env file location: %s", env_path)

        if not env_path:
            raise EnvironmentError("find_dotenv() was unable to find a .env file.")

        load_dotenv(env_path)

        # Extract master LastPass creds from environment
        lastpass_username = os.getenv("LASTPASS_EMAIL")
        lastpass_password = os.getenv("LASTPASS_PASSWORD")

        # Validate that both the email and password values exist
        if not (lastpass_username and lastpass_password):
            raise EnvironmentError(
                ".env does not contain entries for LASTPASS_EMAIL and LASTPASS_PASSWORD."
            )

        # Connect to LastPass: the self.vault object can be reused
        self.vault = lastpass.Vault.open_remote(lastpass_username, lastpass_password)

    def show_keys(self) -> list:
        """Return the list of all available keys."""
        logging.debug("Entered show_keys()")
        return [item.name.decode() for item in self.vault.accounts]

    def get(self, key: str) -> dict:
        """
        Given the name of a LastPass entry (i.e. the key), returns all of the values associated with that key.
        If the key is not found, return an empty dictionary.

        Example response:
        {
            'id': '12345',
            'name': 'SALESFORCE_CREDS',
            'username': 'david@gmail.com',
            'password': 'Salesforce_Password!',
            'url': 'https://system.netsuite.com/pages/customerlogin.jsp?country=US',
            'group': '',
            'notes': ''
        }

        :param key: (str) The name of the LastPass entry.
        :return: (dict) returns all of the values in a dictionary, or an empty dict if an entry doesn't exist.
        """
        logging.debug("Entered get(): key=%s", key)
        for item in self.vault.accounts:
            # If the key is found, return all of its attributes as a dictionary.
            if item.name.decode() == key:
                return {k: v.decode() for k, v in item.__dict__.items()}
        else:
            # Key not found; return empty dictionary.
            return {}
